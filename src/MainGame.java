import javax.swing.JButton;
/**
 * 
 * @author justinparrigin
 * Tic Tac Toe
 * Handler for Main Game Functions
 */
public class MainGame {
	
	public static boolean gameWon = false;
	public static boolean gameStart;
	public static guiHandler gui = new guiHandler();

	public static void main(String[] args) {
		//Variables
		gui.makeWindow();
		gui.createBoard();
		gui.message.setText("Click any square to start game!");
	}
	
	//Begins the game, resetting all values
	public static void StartGame(JButton[] board) {
		for(JButton space : board) {
			space.setText("");
		}
		gui.message2.setVisible(false);
		gameStart = true;
		gui.playerStamp = "X";
		gui.message.setText("Player One's Turn");
	}
	
	//Change Players
	public static void ChangePlayers() {
		if(gui.playerStamp == "X") {
			gui.playerStamp = "O";
			gui.message.setText("Player Two's Turn");
		} else {
			gui.playerStamp = "X";
			gui.message.setText("Player One's Turn");
		}
	}
	
	//If winner
	public static void Winner(String player) {
		gameStart = false;
		String winningMessage = "";
		if(player == "X") {
			winningMessage = "Player One Wins!";
		} else {
			winningMessage = "Player Two Wins!";
		}
		
		gui.message.setText(winningMessage);
		gui.message2.setVisible(true);
		
	}
	
	//If tie
	public static void Tie() {
		gameStart = false;
		gui.message.setText("It's a tie!");
		gui.message2.setVisible(true);
	}

}
