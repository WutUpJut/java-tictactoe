import javax.swing.*;
/**
 * 
 * @author justinparrigin
 * Move Handler for Tic Tac Toe
 */
public class MoveCheck {
	
	//Checks if the spaces are free
	public static Boolean FreeSpaceCheck(JButton button) {
		if(button.getText() == "") {
			return(true);
		} else {
			return(false);
		}
	}
	
	//Checks if there is a player that has won
	public static Boolean WinCheck(JButton[] board, String playerStamp) {
		//Player's Marker 3x
		String playerStampLine = "" + playerStamp + playerStamp + playerStamp;
		//Board's Line Check
		String boardStampLine = "";
		
		//Horizontal Check
		for(int space=0; space<board.length; space++) {
			boardStampLine += board[space].getText();
			if(boardStampLine.contentEquals(playerStampLine)) {
				return(true);
			} else if(space == 2 || space == 5 || space == 8){
				boardStampLine = "";
			}
		}
		
		System.out.println(boardStampLine);
		
		//Vertical Check
		for(int column=0; column<=2; column++) {
			boardStampLine += board[column].getText();
			boardStampLine += board[column+3].getText();
			boardStampLine += board[column+6].getText();
			if(boardStampLine.contentEquals(playerStampLine)) {
				return(true);
			} else {
				boardStampLine = "";
			}
		}
		
		//Diagonal Check
		if(playerStamp.equals(board[0].getText()) && playerStamp.equals(board[4].getText()) && playerStamp.equals(board[8].getText())) {
			return(true);
		} else if (playerStamp.equals(board[2].getText()) && playerStamp.equals(board[4].getText()) && playerStamp.equals(board[6].getText())) {
			return(true);
		}
		
		//Did not win, return false
		return(false);
		
	}
	
	//Checks if there is any more available spaces
	public static Boolean AvailableSpaces(JButton[] board) {
		int filledSpaces = 0;
		for(JButton space : board) {
			String spaceVal = space.getText();
			if(!spaceVal.equals("")) {
				filledSpaces++;
			}
		}
		if(filledSpaces == 9) {
			return(true);
		} else {
			return(false);
		}
	}
}
