import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * 
 * @author justinparrigin
 * GUI Handler for Tic Tac Toe
 */

@SuppressWarnings("serial")
public class guiHandler extends Frame implements ActionListener {
	
	//GUI Instantiation
	private JFrame playerWindow = new JFrame("Tic Tac Toe");
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	//Messenger
	JLabel message = new JLabel("");
	JLabel message2 = new JLabel("Click a square to start again!");
	JLabel rules = new JLabel("Player One : X \n Player Two : O");
	//Buttons
	public JButton topLeftBtn = new JButton();
	public JButton topMiddleBtn = new JButton();
	public JButton topRightBtn = new JButton();
	public JButton middleLeftBtn = new JButton();
	public JButton middleMiddleBtn = new JButton();
	public JButton middleBottomBtn = new JButton();
	public JButton bottomLeftBtn = new JButton();
	public JButton bottomMiddleBtn = new JButton();
	public JButton bottomRightBtn = new JButton();
	public JButton[] boardPieces = {topLeftBtn, topMiddleBtn, topRightBtn, 
                                     middleLeftBtn, middleMiddleBtn, middleBottomBtn,
                                     bottomLeftBtn, bottomMiddleBtn, bottomRightBtn};
	//Player Move
	public String playerStamp;
	
	//Instantiate Windows
	public void makeWindow() {
		playerWindow.setSize(500, 500);
		playerWindow.setResizable(true);
		playerWindow.setLayout(flow);
		playerWindow.setVisible(true);
		playerWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	//Create the GUI with Buttons
	public void createBoard() {
		for(JButton piece : boardPieces) {
			piece.setPreferredSize(new Dimension(75, 75));
			piece.addActionListener(this);
			playerWindow.add(piece);
		}
		playerWindow.add(message);
		message2.setVisible(false);
		playerWindow.add(message2);
		playerWindow.add(rules);
		playerWindow.setSize(250, 350);
	}

	
	//Event Handler
	@Override
	public void actionPerformed(ActionEvent ae) {
		//If the Game is active
		if(MainGame.gameStart == true) {
			//Check if the move is free
			if(MoveCheck.FreeSpaceCheck((JButton)ae.getSource())) {
				JButton chosenButton = (JButton)ae.getSource();
				chosenButton.setText(playerStamp);
				//Check for winning move
				if(MoveCheck.WinCheck(boardPieces, playerStamp)) {
					MainGame.Winner(playerStamp);
				} else {
					//If all the spaces are filled up
					if(MoveCheck.AvailableSpaces(boardPieces)) {
						MainGame.Tie();
					} else {
						//Change Players
						MainGame.ChangePlayers();
					}
				}
			} else {
				message.setText("Spot is already chosen!");
			}
		} else {
			MainGame.StartGame(boardPieces);
		}
	}

}
